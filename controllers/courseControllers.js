// -------------> ACTIVITY 2 <-------------

// [SECTION] DEPENDENCIES
const Course = require("../models/Course");
const bcrypt = require("bcryptjs");

// [SECTION] ADD COURSE
module.exports.addCourse = (req, res) => {

	console.log(req.body);

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};

// [SECTION] GET ALL COURSES
module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// -------------> ACTIVITY 3 <-------------
// [SECTION] GET SINGLE COURSE
module.exports.getSingleCourse = (req, res) => {

	console.log(req.params)

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};
// -------------> END OF ACTIVITY 3 <-------------

// [SECTION] UPDATING A COURSE 

module.exports.updateCourse = (req, res) => {

	let updates = {

		name: req.body.name, 
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};

// -------------> ACTIVITY 4 <-------------

// [SECTION] ARCHIVE A COURSE (Change to Inactive)
module.exports.archiveCourse = (req, res) => {

	let updates = {

		isActive: false
	};

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(archivedCourse => res.send(archivedCourse))
	.catch(err => res.send(err));
}

// [SECTION] ACTIVATE A COURSE (Change to Active)
module.exports.activateCourse = (req, res) => {

	let updates = {

		isActive: true
	};

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err));
}

// [SECTION] GET ACTIVE COURSES
module.exports.getActiveCourses = (req, res) => {

	Course.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// -------------> END OF ACTIVITY 4 <-------------

// [SECTION] FIND COURSES BY NAME
module.exports.findCoursesByName = (req, res) => {

	console.log(req.body) // contain the name of the course you're looking for.

	Course.find({name: {$regex: req.body.name, $options: "$i"}})
	.then(result => {

		if(result.length === 0){
			return res.send("No courses found")

		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
};