// [SECTION] DEPENDENCIES
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcryptjs");
const auth = require("../auth");

// [SECTION] REGISTER USER
module.exports.registerUser = (req, res) => {

	console.log(req.body);

	/*
		bcrypt.hashSync(<stringToBeHashed>, <saltnds>)

		saltRounds --> number of times the characters in the hash are randomized. 

		//sample variable for req.body.password
		let password = req.body.password
	*/
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

// [SECTION] RETRIEVAL OF ALL USERS
module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

// [SECTION] LOGIN USER
module.exports.loginUser = (req, res) => {

	console.log(req.body)

	/*
		1. Find the user by the email
		2. If we find user, we will check the password
		3. If we don't find the user, then we will send a message to the client
		4. If upon checking the found user's password is the same as our input password, we will generate the "token/key" to access our app. If not, we will turn them away by sending a message to the client.
	*/

	User.findOne({email: req.body.email}).then(foundUser => {

		if(foundUser === null){
			return res.send("No user found in the database");

		} else {

			// bcrypt.compareSync() 
			// --> compares 2 values (2 parameters) to see if they are identical.
			// --> will return a boolean value; true if it matches; false if not.
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPasswordCorrect);

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send("Incorrect password, please try again.")
			}
		}
	})
	.catch(err => res.send(err));
};

// [SECTION] GETTING SINGLE USER DETAILS

module.exports.getUserDetails = (req, res) => {

	console.log(req.user);
	/*
		expected output: decoded token
		
		{
		  id: '6296fea2eb3c8105805afa94',
		  email: 'callMeLisa@gmail.com',
		  isADmin: false,
		  iat: 1654136808
		}
	*/

	// find the logged in user's documents from our db and send it to the client by its id.

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// -------------> ACTIVITY 3 <-------------
// [SECTION] CHECK EMAIL EXISTS
module.exports.checkEmailExists = (req, res) => {

	console.log(req.body);

	User.findOne({email: req.body.email}).then(result => {

		if(result === null){
			return res.send("Email is avaialble.")

		} else {

			return res.send("Email is already registered.")
		}
	})
	.catch(err => res.send(err));
};
// -------------> END OF ACTIVITY 3 <-------------

// [SECTION] UPDATING USER DETAILS (NON-ADMIN)
module.exports.updateUserDetails = (req, res) => {

	console.log(req.body); // input for new values
	console.log(req.user.id) // will check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// [SECTION] UPDATE AN ADMIN

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id); // id of the logged in user

	console.log(req.params.id); // id of the user we want to update

	let updates = {

		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));
};

// [SECTION] ENROLL USER

// async --> makes the function asynchronous 

module.exports.enroll = async (req, res) => {

	/*
		Steps:
		1. Look for the user by its id
			-- push the details of the course we're trying to enroll in. We'll push to a new subdocument in our user. 

		2. Look for the course by its id 
			-- push the detail of the user/enrollee who's trying to enroll. We'll push to a new enrollees subdocument in our course. 

		3. When saving of both documents are successful, we send a message to the client.
	*/

	console.log(req.user.id); // the user's id from the decoded token after verify
	console.log(req.body.courseId) // the course from our request body
	console.log(req);

		if(req.user.isAdmin){
			return res.send("Action Forbidden.")
		}

		let isUserUpdated = await User.findById(req.user.id).then(user => {

			console.log(user);
			/*
				{
					id
					firstName
					lastName
					email
					password
					mobileNo
					isAdmin
					enrollments: []
				}
			*/

			let newEnrollment = {
				courseId: req.body.courseId
			}

			user.enrollments.push(newEnrollment)

			return user.save()
			.then(user => true)
			.catch(err => err.message)
		})

		if(isUserUpdated !==  true){
			return res.send({message: isUserUpdated})
		}

		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

			console.log(course);

			/*
				{
					name
					description
					price
					isActive
					createdOn
					enrolees: []
				}
			*/

			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee)

			return course.save()
			.then(course => true)
			.catch(err => err.message)
		})

		if(isCourseUpdated !== true){
			return res.send({message: isCourseUpdated})
		}

		if(isUserUpdated && isCourseUpdated){
			return res.send({message: "User enrolled successfully"})
		}
};

// [SECTION] GET ENROLLMENTS

module.exports.getEnrollments = (req, res) => {

	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(err => res.send(err));
};
