// [SECTION] DEPENDENCIES
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// [SECTION] SERVER
const app = express();
const port = 4000; 

// [SECTION] DATABASE CONNECTION
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.r8ofa.mongodb.net/course-booking-182?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

// [SECTION] NOTIFICATION FOR MONGODB CONNETION

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

// [SECTION] MIDDLEWARES

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

// [SECTION] GROUP ROUTING
//---> for users
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

//---> for courses
const courseRoutes = require("./routes/courseRoutes");
app.use("/courses", courseRoutes);

// [SECTION] PORT LISTENER
app.listen(port, () => console.log(`Server is running at port${port}.`));
