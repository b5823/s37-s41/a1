// [SECTION] DEPENDENCIES
const express = require("express");
const router = express.Router();

// [SECTION] IMPORTED MODULES
const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

//object destructuring from auth module 
const {verify, verifyAdmin} = auth;

console.log(userControllers);

// [SECTION] ROUTES

// register user
router.post("/", userControllers.registerUser);

module.exports = router;

// ---> get all user
/*
	Mini-Activity
	>> endpoint: "/"
	>> Create a route for getting all users
	>> Use the appropriate HTTP methods
	>> Send output in hangouts
*/

router.get("/", userControllers.getAllUsers);

module.exports = router;

// ---> login user
router.post("/login", userControllers.loginUser);

// get user details

router.get("/getUserDetails", verify, userControllers.getUserDetails);

module.exports = router;

// -------------> ACTIVITY 3 <-------------
// ---> check email exists
router.post("/checkEmailExists", userControllers.checkEmailExists);

module.exports = router;
// -------------> END OF ACTIVITY 3 <-------------

// Updating User Details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

module.exports = router;

/*
	Mini-activity
	>> Create a user route which is able to capture the id from its url using route params
		-- This route will only update a regular user to an admin
		-- Test the route in postman
		-- Send your output in Hangouts
*/

// Update Admin
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

// Enroll
router.post("/enroll", verify, userControllers.enroll);

// Get Enrollments
router.get("/getEnrollments", verify, userControllers.getEnrollments);

module.exports = router;
