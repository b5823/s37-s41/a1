// -------------> ACTIVITY 2 <-------------

// [SECTION] DEPENDENCIES
const express = require("express");
const router = express.Router();

// [SECTION] IMPORTED MODULES
const courseControllers = require("../controllers/courseControllers");
const auth = require("../auth");

const {verify, verifyAdmin} = auth;

// [SECTION] ROUTES

// ---> add course
router.post("/", verify, verifyAdmin, courseControllers.addCourse);

module.exports = router;

// ---> get all courses
router.get("/", courseControllers.getAllCourses);

module.exports = router;

// -------------> ACTIVITY 3 <-------------

// ---> get single course
router.get("/getSingleCourse/:id", courseControllers.getSingleCourse);

// -------------> END OF ACTIVITY 3 <-------------

//---> update a course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

module.exports = router;

// -------------> ACTIVITY 4 <-------------

// ---> archive a single course
router.put("/archive/:id", courseControllers.archiveCourse);

// ---> activate a single course
router.put("/activate/:id", courseControllers.activateCourse);

// ---> get all active courses
router.get("/getActiveCourses", courseControllers.getActiveCourses);

// ---> find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

// -------------> END OF ACTIVITY 4 <-------------
