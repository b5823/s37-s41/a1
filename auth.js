// [SECTION] DEPENDCIES
const jwt = require("jsonwebtoken")
const secret = "CourseBookingAPI"

module.exports.createAccessToken = (user) => {
	console.log(user);

	// data --> contains all the information/data you want to have tokens
	// data object --> created to contain some details of user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	console.log(data);

	// sign is a method of jsonwebtoken for creation of token
	// sign - prevents tampering of token
	// secret --> acts like a signature for a particular token
	return jwt.sign(data, secret, {})
};

// NOTES:
/*
	1. You can only get access token when a user logs in in your app with the correct credentials.

	2. As a user, you can only get your own details from your own token from logging in.

	3. JWT is not meant for sensitive data.

	4. JWT is like a passport, you use around the app to access certain features meant for your type of user.
*/

// Goal for verify: To check if token exists or it is not tampered

module.exports.verify = (req, res, next) => {

	// req.headers.authorization - contains jwt/token
	let token = req.headers.authorization;

	// check if the token has been tampered or if there's no token at all. 
	if(typeof token === "undefined"){
		// typeof result is a string

		return res.send({auth: "Failed. No Token"})

	} else {
		console.log(token);

		/*
			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWEyZWIzYzgxMDU4MDVhZmE5NCIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FEbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQ4fQ.ElaNcu-aj1ueGhrk8f1Ox_WrZHadDIU_1T6eop4ABbU

			"Bearer + <white space>" ---> deleted because of the slice function
		*/
		token = token.slice(7, token.length)
		console.log(token)

		/*
			expected output after slicing: 

			eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWEyZWIzYzgxMDU4MDVhZmE5NCIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FEbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQ4fQ.ElaNcu-aj1ueGhrk8f1Ox_WrZHadDIU_1T6eop4ABbU
		*/

		jwt.verify(token, secret, (err, decodedToken) => {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})

			} else {
				console.log(decodedToken);

				req.user = decodedToken

				// next() --> pertains to the next function; let us proceed to the next middleware or controller
				next();
			}
		})
	}
};

// For Verifying An Admin
module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
};
